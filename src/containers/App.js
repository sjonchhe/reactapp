import React, { Component } from 'react';
import './App.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
import WithClass from '../hoc/WithClass';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  constructor(props){
    super(props);
    console.log('[App.js] constructor');
    
  }
  state = {
    persons: [
      { id: 1, name: 'Max', age: 28 },
      { id: 2, name: 'Manu', age: 29 },
      { id: 3,name: 'Stephanie', age: 26 }
    ],
    otherState: 'some other value',
    showPersons: false,
    showCockpit: true
  };

  static getDerivedStateFromProps(props, state)
  {
    console.log('[App.js] getDerivedStateFromProps',props);
    return state;
  }
  
  componentDidMount(){
    console.log('[App.js] componentDidMount');
  }

  shouldComponentUpdate(){
    console.log('[App.js] shouldComponentUpdate');
    return true;
  }
  componentDidUpdate(){
    console.log('[App.js] componentDidUpdate');

  }

  // componentWillMount(){
  //   console.log('[App.js] componentWillMount');
  // }

  nameChangedHandler = (event,id) => {
    //getting index of the person that has been clicked 
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    } ;

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({ persons:persons  });
  }

  togglePersonsHandler = (e) => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow})
  }

  deletePersonHandler =(personIndex) => {
    const persons= this.state.persons.slice();
    persons.splice(personIndex,1);
    this.setState({persons: persons});
  }
 
  render() {
 
      console.log('[App.js] render');
    let persons = null;
    if(this.state.showPersons){
      persons=<Persons 
                persons={this.state.persons}
                clicked={this.deletePersonHandler}
                changed={this.nameChangedHandler} />;      
    } 

   
    return (
      <WithClass classes={App}>
        <button onClick={ () => {
        this.setState({ showCockpit: false }); 
        }}
        >
          Remove Cockpit</button>

        {this.state.showCockpit ? <Cockpit 
                title={this.props.appTitle}
                showPersons={this.state.showPersons}
                personsLength={this.state.persons.length}
                clicked={this.togglePersonsHandler} /> : null}   
        
        { persons }   
      </WithClass>
    );
    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Does this work now?
  }
}

export default App;
