import React, { useEffect } from 'react';
import './Cockpit.css';

const Cockpit = ( props ) => {

  useEffect( () => {
    console.log('[Cockpit.js] useEffect');

    setTimeout( () =>{
      alert('Saved data to cloud')
    },1000);
    return () => {
      console.log ('[Cockpit.js] cleanup work in useEffect');
    }
  },[]);

  useEffect (() => {
    console.log('[Cockpit.js] 2nd useEffect');
    return () => {
      console.log ('[Cockpit.js] cleanup work in 2nd useEffect');
    }
  })

  const style= {
    cursor: 'pointer',
    backgroundColor: 'green',
    color: 'white',
    padding: '10px',
    borderRadius: '5px',
   
   }

   //adding classes depending upon the number of value
   let classes = [];
   
    if(props.showPersons)
    {
      style.backgroundColor='red';
    }
   if(props.personsLength <=2){
     classes.push('red'); //classes =['red']
   } 
   if(props.personsLength <=1) {
     classes.push('bold'); //classes=['red','bold']
   }

  return (
    <div>
      <h1>{props.title}</h1>
        <p className= {classes.join(' ')}>This is working!</p>
        <button style={style}  onClick={props.clicked}> Toggle  </button>
        
    </div>
  )
}

export default React.memo(Cockpit);