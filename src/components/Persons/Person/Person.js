import React, { Component } from 'react';
import Auxiliary from '../../../hoc/Auxiliary';

class Person extends Component {
    render(){
        console.log('[Person.js] rendering...')
        return (
            <Auxiliary>
            <div className="card card-body col-md-5 mb-3 mt-2 mx-auto">
                <p >I'm {this.props.name} and I am {this.props.age} years old! <i className="fas fa-times" onClick={this.props.click} style={{color: "red", float: "right", cursor: "pointer"}}></i></p>
                
                <p>{this.props.children}</p>
                <input type="text" onChange={this.props.changed} value={this.props.name} />
            </div>
            </Auxiliary>
        )
    }
    
};

export default Person;